<?php

require_once(DIR_APPLICATION . 'model/b1/base.php');

class ModelB1Helper extends ModelB1Base
{

    public function printPre($value)
    {
        echo '<pre>';
        print_r($value);
        echo '</pre>';
    }

}
