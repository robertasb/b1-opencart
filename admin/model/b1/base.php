<?php

class ModelB1Base extends Model
{

    public static function productTableName()
    {
        return DB_PREFIX . 'product';
    }

    public static function productDescriptionTableName()
    {
        return DB_PREFIX . 'product_description';
    }

    public static function orderTableName()
    {
        return DB_PREFIX . 'order';
    }

    public static function itemsTableName()
    {
        return DB_PREFIX . 'b1_items';
    }

    public function cronUrl($route, $params = array())
    {
        $this->load->model('b1/settings');
        $this->load->model('b1/items');
        $params['key'] = $this->model_b1_settings->get('cron_key');
        $url = $this->prepareUrl(str_replace('index.php', 'b1_cron.php', $this->url->link($route, http_build_query($params), 'SSL')));
        $url = str_replace('route', 'id', $url);
        return $url;
    }

    protected function adminUrl($route, $params = array())
    {
        $params['token'] = $this->session->data['token'];
        return $this->prepareUrl($this->url->link($route, http_build_query($params), 'SSL'));
    }

    protected function prepareUrl($url)
    {
        return str_replace('&amp;', '&', $url);
    }
}
