<?php

require_once(DIR_APPLICATION . 'model/b1/base.php');

class ModelB1Settings extends ModelB1Base
{

    public static function tableName()
    {
        return DB_PREFIX . 'b1_settings';
    }

    public function createTable()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . self::tableName() . "` (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `date_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            `key` varchar(128) NOT NULL,
            `value` varchar(128) NOT NULL,
            PRIMARY KEY (`id`)
            ) ENGINE=MYISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");
    }

    public function deleteTable()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . self::tableName() . "`");
    }

    public function insertDefaultSettings()
    {
        $this->add('cron_key', $this->generateCronKey());
        $this->add('api_key', '');
        $this->add('private_key', '');
        $this->add('shop_id', base_convert(time(), 10, 36));
        $this->add('b1_id', '');
        $this->add('initial_sync', '0');
        $this->add('documentation_url', 'https://www.b1.lt/doc/api');
        $this->add('help_page_url', 'https://www.b1.lt/help');
        $this->add('b1_contact_email', 'info@b1.lt');
        $this->add('orders_sync_from', date("Y-m-d"));
        $this->add('relation_type', '1_1');
        $this->add('order_status_id', 5);
        $this->add('quantities_next_sync', '');
        $this->add('quantities_sync_count', 0);
        $this->add('products_next_sync', '');
        $this->add('products_sync_count', 0);
    }

    private function generateCronKey()
    {
        return hash_hmac('sha256', uniqid(rand(), true), microtime() . rand());
    }

    public function updateExternalTables()
    {
        $this->db->query("ALTER TABLE `" . self::productTableName() . "` ADD `b1_reference_id` INT(10) UNSIGNED NULL DEFAULT NULL");
        $this->db->query("ALTER TABLE `" . self::productTableName() . "` ADD INDEX (`b1_reference_id`)");
        $this->db->query("ALTER TABLE `" . self::orderTableName() . "` ADD `b1_reference_id` INT(10) UNSIGNED NULL DEFAULT NULL, ADD `next_sync` timestamp NULL DEFAULT NULL,  ADD `b1_sync_count` SMALLINT NOT NULL, ADD `b1_sync_id` INT(11) NULL");
        $this->load->model('user/user_group');
        $this->model_user_user_group->addPermission($this->user->getId(), 'access', 'module/b1');
        $this->model_user_user_group->addPermission($this->user->getId(), 'modify', 'module/b1');
    }

    public function deleteExternalTableModifications()
    {
        $this->db->query("ALTER TABLE `" . self::productTableName() . "` DROP `b1_reference_id`");
        $this->db->query("ALTER TABLE `" . self::orderTableName() . "` DROP `b1_reference_id`, DROP `b1_sync_count`, DROP `next_sync`, DROP `b1_sync_id`");
        $this->load->model('user/user');
        $this->load->model('user/user_group');
        $user = $this->model_user_user->getUser($this->user->getId());
        $userGroupData = $this->model_user_user_group->getUserGroup($user['user_group_id']);
        unset($userGroupData['access']['module/b1']);
        unset($userGroupData['modify']['module/b1']);
        $this->model_user_user_group->editUserGroup($user['user_group_id'], $userGroupData);
    }

    public function get($key)
    {
        $query = $this->db->query("SELECT * FROM `" . self::tableName() . "` WHERE `key` = '" . $this->db->escape($key) . "'");
        if (isset($query->row['value'])) {
            return $query->row['value'];
        } else {
            return null;
        }
    }

    public function set($key, $data)
    {
        $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '" . $this->db->escape($data) . "' WHERE `key` = '" . $this->db->escape($key) . "'");
    }

    public function update($keys, $data)
    {
        foreach ($keys as $key) {
            $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '" . $this->db->escape($data[$key]) . "' WHERE `key` = '" . $this->db->escape($key) . "'");
        }
    }

    public function delete($key)
    {
        $this->db->query("DELETE FROM `" . self::tableName() . "` WHERE `key` = '" . $this->db->escape($key) . "'");
    }

    public function add($key, $value)
    {
        $this->db->query("INSERT INTO `" . self::tableName() . "` SET `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
    }

    public function disable($key)
    {
        $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '0' WHERE `key` = '" . $this->db->escape($key) . "'");
    }

    public function enable($key)
    {
        $this->db->query("UPDATE `" . self::tableName() . "` SET `value` = '1' WHERE `key` = '" . $this->db->escape($key) . "'");
    }

}
