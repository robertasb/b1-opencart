<?php

require_once(DIR_APPLICATION . 'model/b1/base.php');

class ModelB1Items extends ModelB1Base
{

    public static function tableName()
    {
        return DB_PREFIX . 'b1_items';
    }

    public function createTable()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . self::tableName() . "` (`id` INT(11) NOT NULL AUTO_INCREMENT,
        `name` TEXT,
        `code` TEXT,
        `b1_id` INT(11) DEFAULT NULL UNIQUE,
        PRIMARY KEY (`id`)
      ) ENGINE=MYISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1");
    }

    public function deleteTable()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . self::tableName() . "`");
    }

    public function getB1ItemsCount()
    {
        return $this->db->query("SELECT COUNT(*) as count FROM " . DB_PREFIX . "b1_items")->row['count'];
    }

    public function getShopItemsCount()
    {
        return $this->db->query("SELECT COUNT(*) as count FROM " . DB_PREFIX . "product")->row['count'];
    }

    public function getUnlinkedItemsCount()
    {
        $sql = "SELECT COUNT(*) as count FROM `" . DB_PREFIX . "product` LEFT JOIN `" . DB_PREFIX . "product_description` ON " . DB_PREFIX . "product.product_id = " . DB_PREFIX . "product_description.product_id  WHERE `b1_reference_id` IS NULL";
        return $this->db->query($sql)->row['count'];
    }

    public function getLinkedItemsCount()
    {
        $sql = "SELECT COUNT(*) as count FROM `" . DB_PREFIX . "product` LEFT JOIN `" . DB_PREFIX . "product_description` ON " . DB_PREFIX . "product.product_id = " . DB_PREFIX . "product_description.product_id  WHERE `b1_reference_id` IS NOT NULL";
        return $this->db->query($sql)->row['count'];
    }

    public function getB1ItemsCountTable($references)
    {
        return $this->db->query("SELECT COUNT(*) as count FROM " . DB_PREFIX . "b1_items WHERE b1_id NOT IN (" . implode(',', $references) . ") ORDER BY `name` ASC")->row['count'];
    }

    public function unlinkProduct($productId)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET `b1_reference_id` = NULL WHERE `product_id` = '" . $this->db->escape($productId) . "'");
    }

    public function linkProduct($productId, $referenceId)
    {
        if (($this->db->escape($referenceId) != 0) && ($this->db->escape($productId) != 0)) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET `b1_reference_id` = '" . $this->db->escape($referenceId) . "' WHERE `product_id` = '" . $this->db->escape($productId) . "'");
        }
    }

    public function resetProducts()
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET `b1_reference_id` = NULL WHERE `b1_reference_id` IS NOT NULL");
    }

    public function getB1Items($from, $items, $references)
    {
        return $this->db->query("SELECT * FROM " . DB_PREFIX . "b1_items WHERE b1_id NOT IN (" . implode(',', $references) . ") ORDER BY `name` ASC LIMIT " . $this->db->escape($from) . ", " . $this->db->escape($items))->rows;
    }

    public function getUnlinkedItems($from, $items)
    {
        $sql = "SELECT upc, name, b1_reference_id, " . DB_PREFIX . "product.product_id FROM `" . DB_PREFIX . "product` LEFT JOIN `" . DB_PREFIX . "product_description` ON " . DB_PREFIX . "product.product_id = " . DB_PREFIX . "product_description.product_id  WHERE `b1_reference_id` IS NULL ORDER BY `name` ASC LIMIT " . $this->db->escape($from) . ", " . $this->db->escape($items);
        return $this->db->query($sql)->rows;
    }

    public function getLinkedItems($from, $items)
    {
        $sql = "SELECT " . DB_PREFIX . "product_description.name, " . DB_PREFIX . "b1_items.name as b1_name, " . DB_PREFIX . "product_description.product_id, b1_reference_id, upc FROM `" . DB_PREFIX . "product` LEFT JOIN `" . DB_PREFIX . "product_description` ON " . DB_PREFIX . "product.product_id = " . DB_PREFIX . "product_description.product_id LEFT JOIN `" . DB_PREFIX . "b1_items` ON " . DB_PREFIX . "product.b1_reference_id = " . DB_PREFIX . "b1_items.b1_id  WHERE `b1_reference_id` IS NOT NULL ORDER BY " . DB_PREFIX . "product_description.name ASC LIMIT " . $this->db->escape($from) . ", " . $this->db->escape($items);
        return $this->db->query($sql)->rows;
    }

}
