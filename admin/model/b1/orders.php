<?php

require_once(DIR_APPLICATION . 'model/b1/base.php');

class ModelB1Orders extends ModelB1Base
{

    public function getFailedOrders()
    {
        $sql = "SELECT COUNT(*) as count FROM " . DB_PREFIX . "order WHERE b1_sync_id IS NOT NULL";
        return $this->db->query($sql)->row['count'];
    }

    public function getOrderStatuses()
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_status` WHERE language_id = 1 ORDER BY order_status_id");
        return $query->rows;
    }

    public function resetOrdersSync()
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET `b1_reference_id` = NULL WHERE b1_reference_id = 0");
    }

    public function resetOrders()
    {
        $this->db->query("UPDATE `" . DB_PREFIX . "b1_settings` SET `value` = '0' WHERE `key` = 'initial_sync'");
        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET `b1_reference_id` = null, `b1_sync_count` = '0', `b1_sync_id` = null ");
    }
}
