<?php

require_once(DIR_APPLICATION . 'model/b1/base.php');

class ModelB1Clients extends ModelB1Base
{

    public static function tableName()
    {
        return DB_PREFIX . 'b1_clients';
    }

    public function createTable()
    {
        $this->db->query("CREATE TABLE IF NOT EXISTS `" . self::tableName() . "` (
            `id` INTEGER(10) NOT NULL AUTO_INCREMENT,
            `b1_client_id` INTEGER(10) DEFAULT NULL,
            `shop_client_id` INTEGER(10) DEFAULT NULL,
            PRIMARY KEY (`id`),
            UNIQUE KEY `id` (`b1_client_id`, `shop_client_id`)
        ) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
    }

    public function deleteTable()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . self::tableName() . "`");
    }

    public function addClient($b1_client_id, $shop_client_id)
    {
        $sql = "INSERT IGNORE INTO `" . self::tableName() . "` (`b1_client_id`, `shop_client_id`) VALUES ('" . $this->db->escape($b1_client_id) . "' , '" . $this->db->escape($shop_client_id) . "');";
        return $this->db->query($sql);
    }

    public function getB1Client($shop_client_id)
    {
        $sql = "SELECT * FROM `" . self::tableName() . "` WHERE `shop_client_id` =  '" . $this->db->escape($shop_client_id) . "'";
        return $this->db->query($sql)->rows;
    }

}
