<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content" class="b1-extension">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="mailto:<?php echo $url_contact; ?>" data-toggle="tooltip" title="<?php echo $button_contact; ?>" class="btn btn-default">
                    <i class="fa fa-envelope"></i>
                </a>
                <a href="<?php echo $url_documentation; ?>" data-toggle="tooltip" title="<?php echo $button_documentation; ?>" target="_blank" class="btn btn-default">
                    <i class="fa fa-book"></i> <span class="hidden-xs"><?php echo $button_documentation; ?></span>
                </a>
                <a href="<?php echo $url_help_page; ?>" data-toggle="tooltip" title="<?php echo $button_help_page; ?>" target="_blank" class="btn btn-default">
                    <i class="fa fa-book"></i> <span class="hidden-xs"><?php echo $button_help_page ?></span>
                </a>
                <a href="<?php echo $url_cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default">
                    <i class="fa fa-reply"></i>
                </a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#settings" data-toggle="tab"><span class="text-primary"><i class="fa fa-cogs"></i></span> <span class="text-primary hidden-xs"><?php echo $label_settings; ?></span></a></li>
            <li><a href="#unlinked_items" data-toggle="tab"><span class="text-warning"><i class="fa fa-unlink"></i></span> <span class="text-warning hidden-xs"><?php echo $label_unlinked_items; ?></span></a></li>
            <li><a href="#linked_items" data-toggle="tab"><span class="text-success"><i class="fa fa-link"></i></span> <span class="text-success hidden-xs"><?php echo $label_linked_items; ?></a></span></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-cogs"></i> <?php echo $text_configuration; ?></h3>
                            </div>
                            <div class="panel-body">
                                <form action="<?php echo $form_action; ?>" method="post" class="form-horizontal">
                                    <input type="hidden" name="form" value="configuration">
                                    <div class="alert alert-success hidden" id="success-msg" role="alert">
                                        <button type="button" class="close"><span aria-hidden="true">&times;</span></button>
                                        <i class="fa fa-check"></i> <?php echo $text_settings_save_success; ?>
                                    </div>
                                    <?php foreach ($settings as $data) { ?>
                                    <div class="form-group">
                                        <?php if ($data['type'] == 'text') { ?>
                                        <label class="col-sm-3 control-label" for="<?php echo $data['name']; ?>"><?php echo $data['label']; ?>:</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="<?php echo $data['name']; ?>" type="text" value="<?php echo $data['value']; ?>" placeholder="<?php echo $data['label']; ?>">
                                            <div class="error-message"></div>
                                        </div>
                                        <?php } elseif( $data['type'] == 'select') { ?>
                                        <label class="col-sm-3 control-label" for="<?php echo $data['name']; ?>"><?php echo $data['label']; ?>:</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="<?php echo $data['name']; ?>" placeholder="<?php echo $data['label']; ?>">
                                                <option value="1_1"
                                                <?php if($data['value'] == '1_1'){ echo 'selected';} ?>><?php echo $label_relation_1_1; ?></option>
                                                <option value="more_1"
                                                <?php if($data['value'] == 'more_1'){ echo 'selected';} ?>><?php echo $label_relation_more_1; ?></option>
                                            </select>
                                            <div class="error-message"></div>
                                        </div>
                                        <?php } else { ?>
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <div class="checkbox">
                                                <input name="<?php echo $data['name']; ?>" value="0" type="hidden">
                                                <label><input name="<?php echo $data['name']; ?>" <?php echo $data['value'] == 1 ? 'checked' : ''; ?> value="1" type="checkbox"> <?php echo $data['label']; ?></label>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if (isset($data['help'])) { ?>
                                        <div class="col-sm-offset-3 col-sm-9 help-block">
                                            <?php echo $data['help']; ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <div class="alert alert-info">
                                        <?php echo $label_confirmed_status; ?><br>
                                        <?php foreach($order_statuses as $item) { ?>
                                        <?= $item['order_status_id'] ?> - <?= $item['name'] ?><br>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $button_update; ?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-dashboard"></i> <?php echo $text_status; ?></h3>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <?php echo $label_items_in_eshop; ?>
                                    </div>
                                    <div class="col-sm-4 text-right">
                                        <span class="label label-info" id="items-count-eshop"><?= $b1_stat_items_eshop ?></span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <?php echo $label_items_in_b1; ?>
                                    </div>
                                    <div class="col-sm-4 text-right">
                                        <span class="label label-primary" id="items-count-b1"><?= $b1_stat_items_b1 ?></span>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <?php echo $label_orders_failed_to_sync; ?>
                                    </div>
                                    <div class="col-sm-4 text-right">
                                        <span class="label label-danger" id="items-count-order_fail"><?= $b1_stat_failed_orders ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-repeat"></i> <?php echo $text_cron; ?></h3>
                            </div>
                            <div class="list-group">
                                <?php foreach ($cron_urls as $name => $data) { ?>
                                <div class="list-group-item">
                                    <div class="input-group">
                                        <input type="text" class="form-control" value="<?php echo $data['url'] ?>">
                                        <div class="input-group-btn">
                                            <a href="<?php echo $data['url'] ?>" type="button " target="_blank" class="btn btn-primary"><i class="fa fa-play-circle" aria-hidden="true"></i> <?php echo $run_cron; ?></a>
                                        </div>
                                    </div>
                                    <?php 
                                    if ($name == 'fetchQuantities'){
                                    echo $text_cron_description_5;
                                    }else{
                                    echo $text_cron_description;
                                    }
                                    ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <button type="submit" onclick="return reset_all()" class="btn btn-default"><?php echo $reset_all ?></button>
                                <button type="submit" onclick="return reset_all_orders()" class="btn btn-default"><?php echo $reset_all_orders ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="unlinked_items">
                <form id="link" method="POST">
                    <input type="hidden" name="form" value="link_product">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-unlink"></i> <?php echo $title_eshop_items; ?></h3>
                                </div>
                                <div class="panel-body">
                                    <table id="eshop-unlinked_items" class="pagination_shop table table-condensed table-hover" data-source-url="<?php echo $url_eshop_source; ?>">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th><?php echo $label_name; ?></th>
                                            <th><?php echo $label_code; ?></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-2 text-center">
                            <button style="margin-bottom: 15px;" type="submit" class="btn btn-block btn-success">
                                <i class="fa fa-link"></i> <?php echo $text_link; ?>
                            </button>
                        </div>
                        <div class="col-sm-5">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title"><i class="fa fa-unlink"></i> <?php echo $title_b1_items; ?></h3>
                                </div>
                                <div class="panel-body">
                                    <table id="b1-unlinked_items" class="table pagination_b1 table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th><?php echo $label_name; ?></th>
                                            <th><?php echo $label_code; ?></th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <script type="text/javascript">
                    var frm = $('#link');
                    frm.submit(function (ev) {
                        $.ajax({
                            type: frm.attr('method'),
                            url: frm.attr('action'),
                            data: frm.serialize(),
                            success: function (data) {
                                shop = $('input[name=shop_item]:checked').val();
                                b1 = $('input[name=b1_item]:checked').val();
                            }
                        });
                        ev.preventDefault();
                        $('.pagination_shop').DataTable().ajax.reload();
                        $('.pagination_b1').DataTable().ajax.reload();
                    });
                </script>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-info" role="alert"><?php echo $help_unlinked; ?></div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="linked_items">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-link"></i> <?php echo $label_linked_items; ?></h3>
                            </div>
                            <div class="panel-body">
                                <table id="b1-linked_items" class="pagination_linked table table-condensed table-hover" data-source-url="<?php echo $url_source; ?>" data-unlink-url="<?php echo $url_unlink; ?>">
                                    <thead>
                                    <tr>
                                        <th><?php echo $label_eshop_id; ?></th>
                                        <th><?php echo $label_b1_id; ?></th>
                                        <th><?php echo $label_eshop_name; ?></th>
                                        <th><?php echo $label_b1_name; ?></th>
                                        <th><?php echo $label_product_code; ?></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                        <div class="alert alert-info" role="alert"><?php echo $help_linked; ?></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>
<?php 
if($relation == 'more_1'){
    $input_type = 'checkbox';
}else{
    $input_type = 'radio';
} 
?>
<script>
    $(document).ready(function () {
        $('.pagination_shop').DataTable({
            "pagingType": "full_numbers",
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ordering": false,
            "ajax": "<?= $_SERVER['REQUEST_URI'] ?>&type=pagination_shop",
            "columns": [
                {
                    "data": "id",
                    "render": function (data, type, full, meta) {
                        <?php if($input_type == 'checkbox') {?>
                            return '<input type="checkbox" name="shop_item[]" value="' + data + '">';
                        <?php } else { ?>
                            return '<input type="radio" name="shop_item" value="' + data + '">';
                        <?php } ?>
                    }
                },
                {"data": "name"},
                {"data": "code"}
            ]
        });
        $('.pagination_b1').DataTable({
            "pagingType": "full_numbers",
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ordering": false,
            "ajax": "<?= $_SERVER['REQUEST_URI'] ?>&type=pagination_b1",
            "columns": [
                {
                    "data": "id",
                    "render": function (data, type, full, meta) {
                        return '<input type="radio" name="b1_item" value="' + data + '">';
                    }
                },
                {"data": "name"},
                {"data": "code"}
            ]
        });
        $('.pagination_linked').DataTable({
            "pagingType": "full_numbers",
            "processing": true,
            "serverSide": true,
            "searching": false,
            "ordering": false,
            "ajax": "<?= $_SERVER['REQUEST_URI'] ?>&type=pagination_linked",
            "columns": [
                {"data": "id"},
                {"data": "b1_reference_id"},
                {"data": "name"},
                {"data": "b1_name"},
                {"data": "upc"},
                {
                    "data": "id",
                    "render": function (data, type, full, meta) {
                        return '<form class="unlink" method="POST">' +
                            '<input type="hidden" name="form" value="unlink_product">' +
                            '<input type="hidden" name="shop_item" value="' + data + '">' +
                            '<button onclick="return unlink(' + data + ')" type="submit" class="btn btn-success btn-sm"><?php echo $label_unlink ?></button>' +
                            '</form>';
                    }
                }
            ]
        });

    });

    function unlink(id) {
        $.ajax({
            type: 'POST',
            data: {shop_item: id, form: 'unlink_product'},
            success: function (data) {
                $('.pagination_linked').DataTable().ajax.reload();
            }
        });
        return false;
    }
    function reset_all() {
        if (window.confirm("Are you sure?")) {
            $.ajax({
                type: 'POST',
                data: {form: 'reset_all'},
                success: function (data) {
                    $('.pagination_linked').DataTable().ajax.reload();
                }
            });
        }
        return false;
    }
    function reset_all_orders() {
        if (window.confirm("Are you sure?")) {
            $.ajax({
                type: 'POST',
                data: {form: 'reset_all_orders'},
                success: function (data) {
                }
            });
        }
        return false;
    }
    $(document).on("click", "tr", function (e) {
        $(this).find('input:radio').attr('checked', true);
        var chk = $(this).find('input:checkbox').get(0);
        if (chk !== undefined && e.target != chk) {
            chk.checked = !chk.checked;
        }
    });
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if (target = '#unlinked_items') {
            $('.pagination_shop').DataTable().ajax.reload();
            $('.pagination_b1').DataTable().ajax.reload();
        }
        if (target = '#linked_items') {
            $('.pagination_linked').DataTable().ajax.reload();
        }
    });
</script>
<style>
    .dataTable {
        width: 100% !important;
    }
</style>
<?php echo $footer; ?>