<?php

// Version
define('VERSION', '2.3.0.2');

// Configuration
if (is_file('config.php')) {
    require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
    header('Location: ../install/index.php');
    exit;
}

require_once(DIR_SYSTEM . 'library/vendor/b1/libb1/B1.php');
require_once(DIR_SYSTEM . 'library/request.php');
require_once(DIR_SYSTEM . 'library/db.php');
require_once(DIR_SYSTEM . 'library/db/' . DB_DRIVER . '.php');
require_once(DIR_SYSTEM . 'library/config.php');

require_once(DIR_SYSTEM . 'engine/event.php');
require_once(DIR_SYSTEM . 'engine/loader.php');
require_once(DIR_SYSTEM . 'engine/model.php');
require_once(DIR_SYSTEM . 'engine/registry.php');
if (is_file(DIR_SYSTEM . 'engine/proxy.php')) {
    require_once(DIR_SYSTEM . 'engine/proxy.php');
}

class B1CronException extends Exception
{

    private $extraData;

    public function __construct($message = "", $extraData = array(), $code = 0, \Exception $previous = null)
    {
        $this->extraData = $extraData;
        parent::__construct($message, $code, $previous);
    }

    public function getExtraData()
    {
        return $this->extraData;
    }

}

class B1Cron
{

    const TTL = 3600;
    const MAX_ITERATIONS = 300;
    const ORDERS_PER_ITERATION = 300;
    const ORDER_SYNC_THRESHOLD = 10;
    const PLUGIN_NAME = 'Opencart';

    /**
     * @var Request
     */
    private $request;

    /**
     * @var DB
     */
    private $db;

    /**
     * @var ModelB1Settings
     */
    private $settings;

    /**
     * @var ModelB1Helper
     */
    private $helper;

    /**
     * @var ModelB1Clients
     */
    private $clients;

    /**
     * @var B1
     */
    private $library;

    private function init()
    {
        set_time_limit(self::TTL);
        ini_set('max_execution_time', self::TTL);

        // Registry
        $registry = new Registry();
        // Config
        $config = new Config();
        $registry->set('config', $config);
        // Event
        $event = new Event($registry);
        $registry->set('event', $event);
        // Loader
        $loader = new Loader($registry);
        $registry->set('load', $loader);
        // Request
        $registry->set('request', new Request());
        // Database
        $registry->set('db', new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE));

        $this->request = $registry->get('request');
        $this->db = $registry->get('db');

        $loader->model('b1/settings');
        $this->settings = $registry->get('model_b1_settings');
        $loader->model('b1/helper');
        $this->helper = $registry->get('model_b1_helper');
        $loader->model('b1/clients');
        $this->clients = $registry->get('model_b1_clients');
    }

    private function validateAccess()
    {
        $cron_key = $this->settings->get('cron_key');
        if ($cron_key == null) {
            throw new B1CronException('Fatal error.');
        }
        if (isset($this->request->get['key'])) {
            $get_key = $this->request->get['key'];
        } else {
            $get_key = null;
        }
        if ($get_key != $cron_key) {
            exit();
        }
    }

    public function __construct()
    {
        $this->init();
        $this->validateAccess();

        if (isset($this->request->get['id'])) {
            $get_id = $this->request->get['id'];
        } else {
            $get_id = null;
        }
        try {
            $this->library = new B1([
                'apiKey' => $this->settings->get('api_key'),
                'privateKey' => $this->settings->get('private_key')
            ]);
        } catch (B1Exception $e) {
            throw new B1CronException('API/Private key is not provided');
        }

        switch ($get_id) {
            case 'products':
                $this->fetchProducts();
                break;
            case 'quantities':
                $this->fetchQuantities();
                break;
            case 'orders':
                $this->syncOrders();
                break;
            default:
                throw new B1CronException('Bad action ID specified.');
        }
    }

    private function fetchProducts()
    {
        $i = 0;
        $lid = 0;
        $b1_ids = array();
        if ($this->settings->get('products_next_sync') < date('Y-m-d H:i:s') || $this->settings->get('products_next_sync') == '') {
            if ($this->settings->get('products_next_sync') != '') {
                $this->settings->set('products_next_sync', '');
                $this->settings->set('products_sync_count', 0);
            }
            do {
                $productsSyncCount = $this->settings->get('products_sync_count');
                ob_start();
                $i++;
                try {
                    $data = $this->library->exec('shop/product/list', array("lid" => $lid, "pluginName" => self::PLUGIN_NAME));
                    if ($data != false) {
                        foreach ($data['data'] as $item) {
                            $b1_ids[] = $this->db->escape($item['id']);
                            $this->db->query("INSERT IGNORE INTO `" . DB_PREFIX . "b1_items` SET `name` = '" . $this->db->escape($item['name']) . "', `code` = '" . $this->db->escape($item['code']) . "', `b1_id` = '" . $this->db->escape($item['id']) . "'");
                            if ($this->settings->get('relation_type') == '1_1') {
                                if ($item['quantity']) {
                                    $this->db->query("UPDATE " . DB_PREFIX . "product SET `quantity` = " . $this->db->escape($item['quantity']) . " WHERE b1_reference_id = " . $this->db->escape($item['id']));
                                }
                            }
                        }
                        if (count($data['data']) == 100) {
                            $lid = $data['data'][99]['id'];
                        } else {
                            $lid = -1;
                        }
                    } else {
                        $this->settings->set('products_sync_count', $productsSyncCount + 1);
                        throw new B1CronException('Error getting data from B1.lt');
                    }
                } catch (B1Exception $e) {
                    $this->settings->set('products_sync_count', $productsSyncCount + 1);
                    $this->helper->printPre($e->getMessage());
                    $this->helper->printPre($e->getExtraData());
                }
                echo "$i;";
                ob_end_flush();
            } while ($lid != -1 && $i < self::MAX_ITERATIONS && $productsSyncCount < 9);

            if ($this->settings->get('products_sync_count') >= 10) {
                $this->settings->set('products_next_sync', date('Y-m-d H:i:s', strtotime('6 hour')));
            }

            if (!empty($b1_ids)) {
                $this->db->query("UPDATE " . DB_PREFIX . "product SET `b1_reference_id` = NULL WHERE b1_reference_id NOT IN (" . implode(',', $b1_ids) . ")");
                $this->db->query("DELETE FROM  " . DB_PREFIX . "b1_items WHERE b1_id NOT IN (" . implode(',', $b1_ids) . ")");
            }
        }
        echo 'OK';
    }

    private function fetchQuantities()
    {
        if ($this->settings->get('relation_type') == '1_1') {
            echo 'OK';
            exit;
        }
        $i = 0;
        $lid = 0;
        $b1_ids = array();

        if ($this->settings->get('quantities_sync_count') >= 10 && $this->settings->get('quantities_next_sync') == '') {
            $this->settings->set('quantities_next_sync', date('Y-m-d H:i:s', strtotime('6 hour')));
        }
        if ($this->settings->get('quantities_next_sync') < date('Y-m-d H:i:s') || $this->settings->get('quantities_next_sync') == '') {
            if ($this->settings->get('quantities_next_sync') != '') {
                $this->settings->set('quantities_next_sync', '');
                $this->settings->set('quantities_sync_count', 0);
            }
            do {
                $quantitiesSyncCount = $this->settings->get('quantities_sync_count');
                ob_start();
                $i++;
                try {
                    $data = $this->library->exec('shop/product/quantity/list', array("lid" => $lid, "pluginName" => self::PLUGIN_NAME));
                    if ($data != false) {
                        foreach ($data['data'] as $item) {
                            $b1_ids[] = $this->db->escape($item['id']);
                            if ($item['quantity']) {
                                $this->db->query("UPDATE " . DB_PREFIX . "product SET `quantity` = " . $this->db->escape($item['quantity']) . " WHERE b1_reference_id = " . $this->db->escape($item['id']));
                            }
                        }
                        if (count($data['data']) == 100) {
                            $lid = $data['data'][99]['id'];
                        } else {
                            $lid = -1;
                        }
                    } else {
                        $this->settings->set('quantities_sync_count', $quantitiesSyncCount + 1);
                        throw new B1CronException('Error getting data from B1.lt');
                    }
                } catch (B1Exception $e) {
                    $this->settings->set('quantities_sync_count', $quantitiesSyncCount + 1);
                    $this->helper->printPre($e->getMessage());
                    $this->helper->printPre($e->getExtraData());
                }
                echo "$i;";
                ob_end_flush();
            } while ($lid != -1 && $i < self::MAX_ITERATIONS && $quantitiesSyncCount < 9);

            if (!empty($b1_ids)) {
                $this->db->query("UPDATE " . DB_PREFIX . "product SET `b1_reference_id` = NULL WHERE b1_reference_id NOT IN (" . implode(',', $b1_ids) . ")");
            }
        }
        echo 'OK';
    }

    private function syncOrders()
    {
        $id = time();
        try {
            $orders_sync_from = $this->settings->get('orders_sync_from');
            if ($orders_sync_from == null) {
                throw new B1CronException('Not set orders_sync_from value');
            }
            $order_status_id = $this->settings->get('order_status_id');
            if ($order_status_id == null) {
                throw new B1CronException('Not set orders_status_id value');
            }
            $data_prefix = $this->settings->get('shop_id');
            if ($data_prefix == null) {
                throw new B1CronException('Not set shop_id value');
            }
            $initial_sync = $this->settings->get('initial_sync');
            if ($initial_sync == null) {
                throw new B1CronException('Not set initial_sync value');
            }
            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET `next_sync` = NULL, `b1_sync_count` = 0  WHERE next_sync < '" . date('Y-m-d H:i:s') . "'");
            $this->db->query("UPDATE " . DB_PREFIX . "order AS order_table
                LEFT JOIN " . DB_PREFIX . "order_history AS history_table 
                ON order_table.order_id = history_table.order_id
                SET order_table.b1_reference_id = 0
                WHERE order_table.b1_reference_id IS NULL AND history_table.order_status_id = " . $order_status_id . " AND history_table.date_added < '" . $this->db->escape($orders_sync_from) . "'
            ");
            $this->db->query("UPDATE " . DB_PREFIX . "order SET `b1_sync_id` = " . $id . " WHERE ((order_status_id = " . $order_status_id . " AND b1_reference_id IS NULL AND b1_sync_id IS NULL) OR ($id - b1_sync_id > " . self::TTL . " AND b1_sync_count < " . self::ORDER_SYNC_THRESHOLD . ")) AND (next_sync < '" . date('Y-m-d H:i:s') . "' OR next_sync IS NULL)");

            $i = 0;
            do {
                ob_start();
                $i++;
                $processed = 0;
                $orders = $this->db->query("SELECT DISTINCT o.order_id as int_id, o.*, c.*, oh.* FROM " . DB_PREFIX . "order o 
                    LEFT JOIN " . DB_PREFIX . "country c ON o.payment_country_id = c.country_id
                    LEFT JOIN " . DB_PREFIX . "order_history AS oh on o.order_id = oh.order_id AND oh.order_status_id = " . $order_status_id . "
                    WHERE o.b1_sync_id = " . $id . " ORDER BY oh.date_added ASC LIMIT " . self::ORDERS_PER_ITERATION);

                foreach ($orders->rows as $item) {
                    $order_data = $this->generateOrderData($item, $order_status_id, $data_prefix, $initial_sync);
                    try {
                        $request = $this->library->exec('shop/order/add', $order_data['data']);
                        if ($request != false) {
                            if (!isset($order_data['data']['billing']['refid'])) {
                                $this->clients->addClient($request['data']['clientId'], $order_data['customer_id']);
                            }
                            $this->db->query("UPDATE " . DB_PREFIX . "order SET `b1_sync_id` = NULL, `b1_reference_id` = '" . $this->db->escape($request['data']['orderId']) . "' WHERE `order_id` = " . $this->db->escape($item['order_id']));
                        } else {
                            $this->db->query("UPDATE " . DB_PREFIX . "order SET `b1_sync_id` = NULL, `b1_sync_count` = b1_sync_count + 1, `next_sync` = IF(b1_sync_count >= '10', '" . date('Y-m-d H:i:s', strtotime('6 hour')) . "' , NULL) WHERE `order_id` = " . $this->db->escape($item['order_id']));
                        }
                    } catch (B1DuplicateException $e) {
                        $receivedData = $e->getExtraData();
                        $this->db->query("UPDATE " . DB_PREFIX . "order SET `b1_sync_id` = NULL, `b1_sync_count` = b1_sync_count + 1, `next_sync` = IF(b1_sync_count >= '10', '" . date('Y-m-d H:i:s', strtotime('6 hour')) . "' , NULL) WHERE `order_id` = " . $this->db->escape($item['order_id']));
                        $this->db->query("UPDATE " . DB_PREFIX . "order SET `b1_sync_id` = NULL, `b1_reference_id` = '" . $this->db->escape($receivedData['data']['orderId']) . "' WHERE `order_id` = " . $this->db->escape($item['order_id']));
                        $this->helper->printPre($e->getMessage());
                        $this->helper->printPre($e->getExtraData());
                        throw new B1CronException('Error syncing order #' . $this->db->escape($receivedData['data']['orderId']) . ' with B1.lt');
                    } catch (B1Exception $e) {
                        $receivedData = $e->getExtraData();
                        $this->db->query("UPDATE " . DB_PREFIX . "order SET `b1_sync_id` = NULL, `b1_sync_count` = b1_sync_count + 1, `next_sync` = IF(b1_sync_count >= '10', '" . date('Y-m-d H:i:s', strtotime('6 hour')) . "' , NULL) WHERE `order_id` = " . $this->db->escape($item['order_id']));
                        $this->helper->printPre($e->getMessage());
                        $this->helper->printPre($e->getExtraData());
                        throw new B1CronException('Error syncing order #' . $this->db->escape($receivedData['data']['orderId']) . ' with B1.lt');
                    }
                    $processed++;
                    echo "$i-$processed;";
                }
                ob_end_flush();
            } while ($processed == self::ORDERS_PER_ITERATION && $i < self::MAX_ITERATIONS);

            if ($initial_sync == 0) {
                $this->settings->set('initial_sync', 1);
            }

            echo 'OK';
        } catch (B1CronException $e) {
            $this->db->query("UPDATE " . DB_PREFIX . "order SET `b1_sync_id` = NULL WHERE `b1_sync_id` = '$id'");
            $this->helper->printPre($e->getMessage());
            $this->helper->printPre($e->getTrace());
            $this->helper->printPre($e->getExtraData());
        }
    }

    private function generateOrderData($item, $order_status_id, $data_prefix, $initial_sync)
    {
        $order_tax = $this->db->query("SELECT `rate` FROM " . DB_PREFIX . "order_total LEFT JOIN " . DB_PREFIX . "tax_rate ON title = name  WHERE `code` = 'tax' AND `order_id` = " . $this->db->escape($item['order_id']))->row;
        $shipping = $this->db->query("SELECT `value` FROM " . DB_PREFIX . "order_total WHERE `code` = 'shipping' AND `order_id` = " . $this->db->escape($item['order_id']))->row;
        $discount = $this->db->query("SELECT `value` FROM " . DB_PREFIX . "order_total WHERE `code` = 'coupon' AND `order_id` = " . $this->db->escape($item['order_id']))->row;
        $order_status = $this->db->query("SELECT `date_added` FROM " . DB_PREFIX . "order_history WHERE `order_status_id` = " . $this->db->escape($order_status_id) . " AND `order_id` = " . $this->db->escape($item['order_id']))->row;
        if (empty($order_status)) {
            throw new B1CronException('Not found order_history data, for ' . $this->db->escape($item['order_id']) . ' order.', array(
                'order_status_id' => $order_status_id,
                'order' => $item,
            ));
        }

        $order_data = array();
        $client = $this->clients->getB1Client($item['customer_id']);
        if (!empty($client)) {
            $order_data['billing']['refid'] = $client[0]['b1_client_id'];
        }
        $order_data['pluginName'] = self::PLUGIN_NAME;
        $order_data['prefix'] = $data_prefix;
        $order_data['orderId'] = $item['order_id'];
        $order_data['orderDate'] = substr($order_status['date_added'], 0, 10);
        $order_data['orderNo'] = $item['order_id'];
        $order_data['currency'] = $item['currency_code'];
        if (isset($shipping['value'])) {
            $order_data['shippingAmount'] = intval(round($shipping['value'] * $item['currency_value'] * 100));
        }
        if (isset($discount['value'])) {
            $order_data['discount'] = intval(round(abs($discount['value']) * $item['currency_value'] * 100));
        } else {
            $order_data['discount'] = 0;
        }
        $order_data['total'] = intval(round($item['total'] * $item['currency_value'] * 100));
        $order_data['orderEmail'] = $item['email'];
        $order_data['vatRate'] = intval(round($order_tax['rate']));
        $order_data['writeOff'] = $initial_sync;
        $order_data['billing']['name'] = empty($item['payment_company']) ? trim($item['payment_firstname'] . ' ' . $item['payment_lastname']) : $item['payment_company'];
        $order_data['billing']['isCompany'] = $item['payment_company'] == '' ? 0 : 1;
        $order_data['billing']['address'] = $item['payment_address_1'];
        $order_data['billing']['city'] = $item['payment_city'];
        $order_data['billing']['country'] = $item['iso_code_2'];

        $order_data['delivery']['name'] = empty($item['shipping_company']) ? trim($item['shipping_firstname'] . ' ' . $item['shipping_lastname']) : $item['shipping_company'];
        $order_data['delivery']['isCompany'] = $item['shipping_company'] == '' ? 0 : 1;
        $order_data['delivery']['address'] = $item['shipping_address_1'];
        $order_data['delivery']['city'] = $item['shipping_city'];
        $order_data['delivery']['country'] = $item['iso_code_2'];
        if ($order_data['billing']['name'] == '') {
            $order_data['billing'] = $order_data['delivery'];
        }
        if ($order_data['delivery']['name'] == '') {
            $order_data['delivery'] = $order_data['billing'];
        }
        $order_products = $this->db->query("SELECT op.*, p.b1_reference_id FROM " . DB_PREFIX . "order_product op LEFT OUTER JOIN " . DB_PREFIX . "product p ON p.product_id = op.product_id WHERE order_id = " . $this->db->escape($item['order_id']));
        if (empty($order_products->rows)) {
            throw new B1CronException('Not found order_products data, for ' . $this->db->escape($item['order_id']) . ' order.', array('order_data' => $order_data));
        }
        foreach ($order_products->rows as $key => $product) {
            if ($this->settings->get('relation_type') == '1_1') {
                $order_data['items'][$key]['id'] = $product['b1_reference_id'];
            }
            $order_data['items'][$key]['name'] = $product['name'];
            $order_data['items'][$key]['quantity'] = intval(round($product['quantity'] * 100));
            $order_data['items'][$key]['price'] = intval(round($product['price'] * $item['currency_value'] * 100));
            $order_data['items'][$key]['sum'] = intval(round($product['price'] * $item['currency_value'] * $product['quantity'] * 100));
        }
        return [
            'customer_id' => $item['customer_id'],
            'data' => $order_data
        ];
    }

}

new B1Cron;
