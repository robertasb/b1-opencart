<?php

require_once(DIR_SYSTEM . 'library/vendor/b1/libb1/B1.php');
require_once(DIR_SYSTEM . 'library/db.php');
require_once(DIR_SYSTEM . 'library/db/' . DB_DRIVER . '.php');

class ControllerExtensionModuleB1Accounting extends Controller
{

    public function index()
    {
        /* Load language file. */
        $this->load->language('module/b1accounting');
        $this->load->model('b1/settings');
        $this->load->model('b1/items');
        $this->load->model('b1/orders');
        $this->load->model('catalog/product');

        $this->document->setTitle($this->language->get('heading_title'));

        /* Check if data has been posted back. */
        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            if ($this->request->post['form'] == 'configuration') {
                $old_sync_from = $this->model_b1_settings->get('orders_sync_from');
                $check = array('b1_id', 'shop_id', 'relation_type', 'orders_sync_from', 'order_status_id', 'api_key', 'private_key', 'cron_key', 'invoice_product_name_mask', 'enable_quantity_sync');
                $this->model_b1_settings->update($check, $this->request->post);
                if ($this->model_b1_settings->get('orders_sync_from') != $old_sync_from) {
                    $this->model_b1_orders->resetOrdersSync();
                }
            }
            if ($this->request->post['form'] == 'reset_all') {
                $this->model_b1_items->resetProducts();
                exit();
            }
            if ($this->request->post['form'] == 'reset_all_orders') {
                $this->model_b1_orders->resetOrders();
                exit();
            }
            if ($this->request->post['form'] == 'link_product') {
                if (is_array($this->request->post['shop_item'])) {
                    foreach ($this->request->post['shop_item'] as $item) {
                        $this->model_b1_items->linkProduct($item, $this->db->escape($this->request->post['b1_item']));
                    }
                } else {
                    $this->model_b1_items->linkProduct($this->request->post['shop_item'], $this->db->escape($this->request->post['b1_item']));
                }
                exit();
            }
            if ($this->request->post['form'] == 'unlink_product') {
                $this->model_b1_items->unlinkProduct($this->request->post['shop_item']);
                exit();
            }
            $this->response->redirect($this->url->link('extension/module/b1accounting', 'token=' . $this->session->data['token'], 'SSL'));
        }

        /* AJAX pagination */
        if (isset($this->request->get['draw'])) {
            $pagination_data = array();
            $pagination_data['draw'] = $this->request->get['draw'];
            $pagination_data['type'] = $this->request->get['type'];
            $pagination_data['start'] = $this->request->get['start'];
            $pagination_data['length'] = $this->request->get['length'];

            if ($pagination_data['type'] == 'pagination_shop') {
                $ajax_sorting_data = $this->model_b1_items->getUnlinkedItems($pagination_data['start'], $pagination_data['length']);
                $total = $this->model_b1_items->getUnlinkedItemsCount();
                foreach ($ajax_sorting_data as $item) {
                    $pagination_data['data'][] = array('name' => $item['name'], 'id' => $item['product_id'], 'code' => $item['upc']);
                }
            }
            if ($pagination_data['type'] == 'pagination_b1') {
                $products_linked_db_values = array();
                $products_linked_db_values[] = 0;
                if ($this->model_b1_settings->get('relation_type') != 'more_1') {
                    foreach ($this->model_catalog_product->getProducts() as $product) {
                        if ($product['b1_reference_id'] != null) {
                            $products_linked_db_values[] = $product['b1_reference_id'];
                        }
                    }
                }
                $ajax_sorting_data = $this->model_b1_items->getB1Items($pagination_data['start'], $pagination_data['length'], $products_linked_db_values);
                $total = $this->model_b1_items->getB1ItemsCountTable($products_linked_db_values);
                foreach ($ajax_sorting_data as $item) {
                    $pagination_data['data'][] = array('name' => $item['name'], 'id' => $item['b1_id'], 'code' => $item['code']);
                }
            }
            if ($pagination_data['type'] == 'pagination_linked') {
                $ajax_sorting_data = $this->model_b1_items->getLinkedItems($pagination_data['start'], $pagination_data['length']);
                $total = $this->model_b1_items->getLinkedItemsCount();
                foreach ($ajax_sorting_data as $item) {
                    $pagination_data['data'][] = array('name' => $item['name'], 'b1_name' => $item['b1_name'], 'id' => $item['product_id'], 'b1_reference_id' => $item['b1_reference_id'], 'upc' => $item['upc']);
                }
            }

            $pagination_data['recordsTotal'] = $total;
            $pagination_data['recordsFiltered'] = $total;

            if ($pagination_data['recordsTotal'] == 0) {
                $pagination_data['data'] = array();
            }
            echo json_encode($pagination_data);
            exit();
        }

        /* Load language strings. */
        $data['label_confirmed_status'] = $this->language->get('label_confirmed_status');
        $data['heading_title'] = $this->language->get('heading_title');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_help_page'] = $this->language->get('button_help_page');
        $data['button_documentation'] = $this->language->get('button_documentation');
        $data['button_fetch_items_from_b1'] = $this->language->get('button_fetch_items_from_b1');
        $data['button_sync_orders_without_writeoff'] = $this->language->get('button_sync_orders_without_writeoff');
        $data['button_update'] = $this->language->get('button_update');
        $data['button_contact'] = $this->language->get('button_contact');
        $data['label_unlink'] = $this->language->get('label_unlink');
        $data['label_settings'] = $this->language->get('label_settings');
        $data['label_unlinked_items'] = $this->language->get('label_unlinked_items');
        $data['label_linked_items'] = $this->language->get('label_linked_items');
        $data['label_failed_to_sync_orders'] = $this->language->get('label_failed_to_sync_orders');
        $data['label_eshop_id'] = $this->language->get('label_eshop_id');
        $data['label_b1_id'] = $this->language->get('label_b1_id');
        $data['label_eshop_name'] = $this->language->get('label_eshop_name');
        $data['label_b1_name'] = $this->language->get('label_b1_name');
        $data['label_b1_code'] = $this->language->get('label_b1_code');
        $data['label_sync_status'] = $this->language->get('label_sync_status');
        $data['label_items_in_eshop'] = $this->language->get('label_items_in_eshop');
        $data['label_items_in_b1'] = $this->language->get('label_items_in_b1');
        $data['label_orders_failed_to_sync'] = $this->language->get('label_orders_failed_to_sync');
        $data['title_eshop_items'] = $this->language->get('title_eshop_items');
        $data['title_b1_items'] = $this->language->get('title_b1_items');
        $data['text_configuration'] = $this->language->get('text_configuration');
        $data['text_cron_description'] = $this->language->get('text_cron_description');
        $data['text_cron_description_5'] = $this->language->get('text_cron_description_5');
        $data['text_link'] = $this->language->get('text_link');
        $data['text_settings_save_success'] = $this->language->get('text_settings_save_success');
        $data['text_cron'] = $this->language->get('text_cron');
        $data['run_cron'] = $this->language->get('run_cron');
        $data['text_status'] = $this->language->get('text_status');
        $data['cron_urls'] = $this->language->get('cron_urls');
        $data['label_id'] = $this->language->get('label_id');
        $data['label_name'] = $this->language->get('label_name');
        $data['label_code'] = $this->language->get('label_code');
        $data['label_shop_identifier'] = $this->language->get('label_shop_identifier');
        $data['orders_sync_from'] = $this->language->get('orders_sync_from');
        $data['order_status_id'] = $this->language->get('order_status_id');
        $data['label_customer'] = $this->language->get('label_customer');
        $data['label_product_code'] = $this->language->get('label_product_code');
        $data['label_total'] = $this->language->get('label_total');
        $data['label_status'] = $this->language->get('label_status');
        $data['help_linked'] = $this->language->get('help_linked');
        $data['reset_all'] = $this->language->get('reset_all');
        $data['reset_all_orders'] = $this->language->get('reset_all_orders');
        $data['help_unlinked'] = $this->language->get('help_unlinked');
        $data['label_relation_1_1'] = $this->language->get('label_relation_1_1');
        $data['label_relation_more_1'] = $this->language->get('label_relation_more_1');
        $data['url_cancel'] = $this->url->link('extension/extension', 'token = ' . $this->session->data['token'], 'SSL');
        $data['url_documentation'] = $this->model_b1_settings->get('documentation_url');
        $data['url_help_page'] = $this->model_b1_settings->get('help_page_url');
        $data['url_contact'] = $this->model_b1_settings->get('b1_contact_email');
        $data['url_source'] = $this->url->link('module/b1/item/linked/data', 'token = ' . $this->session->data['token'], 'SSL');
        $data['url_eshop_source'] = $this->url->link('extension/module/b1accounting/unlinked_data', 'eshop = 1&token = ' . $this->session->data['token'], 'SSL');
        $data['url_unlink'] = $this->url->link('module/b1/item/linked/unlink', 'token = ' . $this->session->data['token'], 'SSL');

        /* Loading up some URLS. */
        $data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
        $data['form_action'] = $this->url->link('extension/module/b1accounting', 'token=' . $this->session->data['token'], 'SSL');
        $data['form_action'] = str_replace('amp;', '', $data['form_action']);

        /* Present error messages to users. */
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        /* Breadcrumb. */
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token = ' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_module'),
            'href' => $this->url->link('extension/extension', 'token = ' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/b1accounting', 'token = ' . $this->session->data['token'], 'SSL')
        );

        /* Crons urls */
        $data['cron_urls'] = array(
            'fetchItemsFromB1' => array(
                'url' => $this->model_b1_settings->cronUrl('products'),
            ),
            'fetchQuantities' => array(
                'url' => $this->model_b1_settings->cronUrl('quantities'),
            ),
            'syncOrdersWithB1' => array(
                'url' => $this->model_b1_settings->cronUrl('orders'),
            ),
        );

        /* Render some output. */
        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        $data['settings'] = $this->load->controller('module/b1/settings');
        $data['unlinked'] = $this->load->controller('module/b1/unlinked');
        $data['linked'] = $this->load->controller('module/b1/linked');
        $data['fail'] = $this->load->controller('module/b1/fail');

        /* B1 settings */
        $data['settings'] = array(
            array(
                'type' => 'text',
                'name' => 'shop_id',
                'label' => $this->language->get('label_shop_identifier'),
                'value' => $this->model_b1_settings->get('shop_id'),
            ),
            array(
                'type' => 'text',
                'name' => 'orders_sync_from',
                'label' => $this->language->get('orders_sync_from'),
                'value' => $this->model_b1_settings->get('orders_sync_from'),
            ),
            array(
                'type' => 'text',
                'name' => 'cron_key',
                'label' => $this->language->get('label_cron_key'),
                'value' => $this->model_b1_settings->get('cron_key'),
            ),
            array(
                'type' => 'text',
                'name' => 'api_key',
                'label' => $this->language->get('label_api_key'),
                'value' => $this->model_b1_settings->get('api_key'),
            ),
            array(
                'type' => 'text',
                'name' => 'private_key',
                'label' => $this->language->get('label_private_key'),
                'value' => $this->model_b1_settings->get('private_key'),
            ),
            array(
                'type' => 'select',
                'name' => 'relation_type',
                'label' => $this->language->get('label_relation_type'),
                'value' => $this->model_b1_settings->get('relation_type'),
            ),
            array(
                'type' => 'text',
                'name' => 'order_status_id',
                'label' => $this->language->get('order_status_id'),
                'value' => $this->model_b1_settings->get('order_status_id'),
            ),
        );

        $data['relation'] = $this->model_b1_settings->get('relation_type');
        $data['b1_stat_items_eshop'] = $this->model_b1_items->getShopItemsCount();
        $data['b1_stat_items_b1'] = $this->model_b1_items->getB1ItemsCount();
        $data['b1_stat_failed_orders'] = $this->model_b1_orders->getFailedOrders();
        $data['order_statuses'] = $this->model_b1_orders->getOrderStatuses();
        $this->response->setOutput($this->load->view('module/b1accounting.tpl', $data));
    }

    public function install()
    {
        $this->load->model('setting/setting');
        $this->load->model('b1/settings');
        $this->load->model('b1/items');
        $this->load->model('b1/clients');
        $this->model_b1_items->createTable();
        $this->model_b1_clients->createTable();
        $this->model_b1_settings->createTable();
        $this->model_b1_settings->insertDefaultSettings();
        $this->model_b1_settings->updateExternalTables();
        $this->model_extension_event->addEvent('b1accounting', 'admin/controller/sale/order/invoice/before', 'extension/module/b1accounting/invoice');
        $this->model_extension_event->addEvent('b1accounting_invoice', 'catalog/controller/account/order/info/after', 'extension/module/b1accounting/invoice');

        $settings = $this->model_setting_setting->getSetting('b1');
        $settings['b1_status'] = 1;
        $this->model_setting_setting->editSetting('b1', $settings);
    }

    public function uninstall()
    {
        $this->load->model('setting/setting');
        $this->load->model('b1/settings');
        $this->load->model('b1/items');
        $this->load->model('b1/clients');
        $this->model_b1_settings->deleteTable();
        $this->model_b1_items->deleteTable();
        $this->model_b1_clients->deleteTable();
        $this->model_b1_settings->deleteExternalTableModifications();
        $this->model_extension_event->deleteEvent('b1accounting');
        $this->model_extension_event->deleteEvent('b1accounting_invoice');

        $settings = $this->model_setting_setting->getSetting('b1');
        $settings['b1_status'] = 0;
        $this->model_setting_setting->editSetting('b1', $settings);
    }

}
