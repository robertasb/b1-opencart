Įskiepis skirtas sinchronizuoti produktus tarp OpenCart ir B1.lt aplikacijos.

### Reikalavimai ###

* PHP 5.5
* OpenCart 2.3.0.2
* MySQL 5.7.15

### Diegimas ###

* NEBŪTINA Pasidarykite failų atsarginę kopiją.
* Padarykite atsarginę DB kopiją.
* Perkelkite `admin`, `system`, `invoice.php` direktorijas į opencart direktoriją.
* Atlikite žingsnius žėmiau nurodytose pastabose pagal Jūsų turima Opencart parduotuvės versiją.
* Administracijos skiltyje įdiekite modulį ir suveskite reikiamą informaciją.
* Administracijos skiltyje 'Orders sync from' laukelyje nurodykite data, nuo kurios bus sinchromnizuojami užsakymai. Datos formatas Y-m-d. Pvz. 2016-12-10 
* Administracijos skiltyje 'Confirmed order status ID' laukelyje nurodykite užsakymų, kurie bus sinchronizuojami su B1, statuso ID. Pagal nutylėjimą 5 (Complete)
* Administracijos skiltyje 'Items relations for link' nurodykite prekių susiejimo būdą:
    * One to one - surišama vieną parduotuvės prekė su viena B1 preke, kiekiai yra sinchronizuojami
    * More to one - surišamos keletas parduotuvės prekių su viena B1 preke, kiekiai nėra sinchronizuojami
* Prie serverio Cron darbų sąrašo pridėkite visus išvardintus cron darbus, nurodytus modulio konfigūravimo puslapyje.
  Pridėti cron darbus galite per serverio valdymo panelė (DirectAdmin, Cpanel) arba įvykdę šias komandinės eilutės serverio pusėje
    * `0 */12 * * * wget -q -O - '[products_cron_url]'` Vietoj [products_cron_url] reikia nurodyti savo Cron adresą. 
    * `*/5 * * * * wget -q -O - '[orders_cron_url]'` Vietoj [orders_cron_url] reikia nurodyti savo Cron adresą. 
    * `0 */4 * * *  wget -q -O - '[quantities_cron_url]'` Vietoj [quantities_cron_url] reikia nurodyti savo Cron adresą. 
* Susiekite B1 ir e.parduotuvės prekės "Nesusiję produktai" skiltyje.
* Įvykdykite cron prekių kiekio sinchronizacijai.

### Pastabos 2.3.x versijoms ###

* Norėdami, kad pirkėjai matytų B1 sugeneruotas sąskaitas, reikia `catalog\view\theme\default\template\account\order_info.tpl` ( priklausomai nuo Jūsų temos vietoj `default` gali būti kitoks pavadinimas) faile įterpti norimoje puslapio vietoje nuorodą su $b1_invoice_link kintamuoju (PDF sąskaitos adresas) pvz. `<a target="_new" class="btn btn-default" href='<?php echo $b1_invoice_link ?>'>PDF</a>`
* Ištrinkite iš šakninio katalogo invoice.php failą.
* Ištrinkite `admin/controller/module` aplankalą.
* Ištrinkite `admin/language/english` aplankalą.

### Pastabos 2.0.x - 2.2.x versijoms ###

* Norėdami, kad pirkėjai matytų B1 sugeneruotas sąskaitas, reikia `catalog\view\theme\default\template\account\order_info.tpl` ( priklausomai nuo Jūsų temos vietoj `default` gali būti kitoks pavadinimas) faile įterpti norimoje puslapio vietoje nuorodą su '<?php echo HTTP_SERVER . 'index.php?order_id=' . $order_id . '&route=extension/module/b1accounting/invoice' ?>' href atributu  `<a target="_new" class="btn btn-default" href='<?php echo HTTP_SERVER . 'index.php?order_id=' . $order_id . '&route=extension/module/b1accounting/invoice' ?>'>PDF</a>`
* Ištrinkite `admin/controller/extension` aplankalą.

### Kontaktai ###

* Kilus klausimams, prašome kreiptis info@b1.lt